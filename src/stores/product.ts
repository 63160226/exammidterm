export default class Product {
  constructor(public id: number, public name: string, public price: number) {}

  getId(): number {
    return this.id;
  }
  getName(): string {
    return this.name;
  }
  getPrice(): number {
    return this.price;
  }
}
